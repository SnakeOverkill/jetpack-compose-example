package com.snake.jetpackcomposeexample

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.KeyboardArrowLeft
import androidx.compose.material.icons.outlined.KeyboardArrowRight
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material.icons.sharp.Email
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import coil.size.Size
import com.snake.jetpackcomposeexample.ui.theme.JetPackComposeExampleTheme
import com.snake.jetpackcomposeexample.ui.theme.spanStyleBigText
import com.snake.jetpackcomposeexample.ui.theme.spanStyleSmallText
import com.snake.jetpackcomposeexample.ui.theme.textStyleCustom

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            JetPackComposeExampleTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    BoxScreen()
                }
            }
        }
    }
}

//Text Component

@Composable
fun TextScreen() {
    Column(modifier = Modifier.padding(16.dp)) {
        Greeting(
            name = "Snake Learning Jetpack Compose",
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        MultipleStylesText()
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        style = textStyleCustom,
        maxLines = 2,
        overflow = TextOverflow.Ellipsis,
        modifier = modifier
    )
}

@Composable
fun MultipleStylesText() {
    Text(text = buildAnnotatedString {
        withStyle(style = spanStyleBigText) {
            append("J")
        }
        withStyle(style = spanStyleSmallText) {
            append("etpack")
        }
        append(" ")
        withStyle(style = spanStyleBigText) {
            append("C")
        }
        withStyle(style = spanStyleSmallText) {
            append("ompose")
        }
    }, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
}

//Image Component

@Composable
fun ImageScreen() {
    Column(modifier = Modifier.padding(16.dp)) {
        ResourceImageCompose(contentScale = ContentScale.Inside)
        Spacer(modifier = Modifier.height(16.dp))
        CircleImageCompose()
        Spacer(modifier = Modifier.height(16.dp))
        UrlImageCompose()
        Spacer(modifier = Modifier.height(16.dp))
        VectorImageCompose()
        Spacer(modifier = Modifier.height(16.dp))
        CustomPainterImageCompose()
    }
}

@Composable
fun ResourceImageCompose(contentScale: ContentScale) {
    Image(
        painterResource(id = R.drawable.sneaker),
        contentDescription = "Banner Image",
        modifier = Modifier
            .fillMaxWidth()
            .shadow(
                elevation = 8.dp,
                shape = RoundedCornerShape(16.dp)
            ),
        contentScale = contentScale,
        alignment = Alignment.Center
    )
}

@Composable
fun CircleImageCompose() {
    Surface(
        modifier = Modifier
            .border(BorderStroke(2.dp, color = Color.Gray), shape = CircleShape)
            .size(200.dp)
    ) {
        Image(
            painterResource(id = R.drawable.compose_avatar),
            contentDescription = "Avatar Image",
            contentScale = ContentScale.Crop,
        )
    }
}

@Composable
fun VectorImageCompose() {
    Image(
        imageVector = Icons.Filled.Person,
        contentDescription = "Person Image"
    )
}

@Composable
fun CustomPainterImageCompose() {
    Image(
        ColorPainter(Color.Green),
        contentDescription = "Painter Image",
        modifier = Modifier.size(100.dp)
    )
}

@Composable
fun UrlImageCompose() {
    val painter = ImageRequest.Builder(LocalContext.current)
        .data("https://i.pinimg.com/736x/a0/74/c7/a074c71043d8d8c8a57ed469f9b14a0b.jpg")
        .crossfade(1000)
        .build()
    AsyncImage(
        painter,
        placeholder = painterResource(R.drawable.ic_launcher_background),
        contentDescription = "Url Image",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(200.dp)
            .clip(CircleShape)
    )
}

//Button Component

@Composable
fun ButtonScreen() {
    val mContext = LocalContext.current
    val count = remember {
        mutableIntStateOf(0)
    }
    Column(modifier = Modifier
        .padding(16.dp)
        .fillMaxWidth()) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            SimpleButtonComponent(count)
        }
        Text(text = "Count: ${count.intValue}")
    }
}

@Composable
fun SimpleButtonComponent(count: MutableIntState) {
    Button(
        onClick = {
            count.intValue++
        },
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Red,
            contentColor = Color.Yellow
        )
    ) {
        Text(text = "Click Count")
    }
}

@Composable
fun DisableButtonComponent() {
    Button(
        onClick = {},
        colors = ButtonDefaults.buttonColors(
            disabledContainerColor = Color.Gray,
            disabledContentColor = Color.White
        ),
        enabled = false
    ) {
        Text(text = "Disable Button")
    }
}

@Composable
fun IconButtonComponent(context: Context) {
    Button(
        onClick = {
            Toast.makeText(context, "Added To Card!", Toast.LENGTH_SHORT).show()
        },
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Red,
            contentColor = Color.White
        )
    ) {
        Icon(Icons.Default.ShoppingCart, contentDescription = "Add To Button")
        Text(text = "Add To Card")
    }
}

@Composable
fun CornerButtonComponent(context: Context) {
    Button(
        onClick = {
            Toast.makeText(context, "Clicked Me!", Toast.LENGTH_SHORT).show()
        },
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Red,
            contentColor = Color.White
        ),
        shape = RoundedCornerShape(
            topStart = 16.dp,
            bottomEnd = 16.dp
        )
    ) {
        Text(text = "Corner Button")
    }
}

@Composable
fun NegativeButtonComponent(context: Context) {
    Button(
        onClick = {
            Toast.makeText(context, "Clicked Me!", Toast.LENGTH_SHORT).show()
        },
        border = BorderStroke(
            width = 2.dp,
            color = Color.Red
        ),
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Transparent,
            contentColor = Color.Red
        )
    ) {
        Text(text = "Negative Button")
    }
}

@Composable
fun ElevationButtonComponent() {
    Button(onClick = { },
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Red,
            contentColor = Color.White
        ),
        elevation = ButtonDefaults.elevatedButtonElevation(
            defaultElevation = 8.dp,
            pressedElevation = 12.dp,
            disabledElevation = 0.dp
        )
    ) {
        Text(text = "Elevation Button")
    }
}

@Composable
fun OtherButtonComponent() {
    OutlinedButton(onClick = {}) {

    }
    TextButton(onClick = {}) {

    }
    IconButton(onClick = {}) {
        
    }
}

@Composable
fun DetectTapGesturesCompose() {
    val textContent = remember {
        mutableStateOf("")
    }
    Column {
        Text(text = textContent.value)
        Text(
            text = "Something",
            modifier = Modifier.pointerInput(Unit) {
                detectTapGestures(
                    onDoubleTap = {
                        textContent.value = "onDoubleTap"
                    },
                    onPress = {
                        textContent.value = "onPress"
                    },
                    onLongPress = {
                        textContent.value = "onLongPress"
                    }
                )
            }
        )
    }
}

//Radio Button Component

@Composable
fun RadioButtonScreen() {
    Column(modifier = Modifier
        .padding(16.dp)
        .fillMaxSize()
    ) {
        RadioButtonComponent()
    }
}

@Composable
fun RadioButtonComponent() {
    RadioButtonWithTitleComponent(title = "Normal")
    CommonSpace()
    CustomRadioButtonComponent(title = "Custom")
}

@Composable
fun RadioButtonWithTitleComponent(title: String) {
    var isSelected by remember {
        mutableStateOf(false)
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.selectable(
            selected = isSelected,
            onClick = {isSelected = !isSelected},
            role = Role.RadioButton
        )
    ) {
        RadioButton(selected = isSelected, onClick = null, colors = RadioButtonDefaults.colors(
            selectedColor = Color.Red,
            unselectedColor = Color.Black,
            disabledSelectedColor = Color.Gray,
            disabledUnselectedColor = Color.Gray
        ))
        Text(text = title, modifier = Modifier.padding(start = 8.dp))
    }
}

@Composable
fun CustomRadioButtonComponent(title: String) {
    var isSelected by remember {
        mutableStateOf(false)
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.selectable(
            selected = isSelected,
            onClick = {isSelected = !isSelected},
            role = Role.RadioButton
        )
    ) {
        val iconRadio = if (isSelected) Icons.Default.CheckCircle else Icons.Default.Check
        Icon(iconRadio, contentDescription = "Custom Radio Button")
        Text(text = title, modifier = Modifier.padding(start = 8.dp))
    }
}

//Checkbox Component

@Composable
fun CheckboxScreen() {
    Column(modifier = Modifier
        .padding(16.dp)
        .fillMaxSize()
    ) {
        CheckboxComponent("Apple")
    }
}

@Composable
fun CheckboxComponent(title: String) {
    var isSelected by remember {
        mutableStateOf(false)
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.selectable(
            selected = isSelected,
            onClick = {isSelected = !isSelected},
            role = Role.Checkbox
        )
    ) {
        Checkbox(checked = isSelected,
            onCheckedChange = null,
            colors = CheckboxDefaults.colors(
                checkedColor = Color.Red,
                uncheckedColor = Color.Black,
                disabledCheckedColor = Color.Gray,
                disabledUncheckedColor = Color.Gray
            )
        )
        Text(text = title, modifier = Modifier.padding(start = 8.dp))
    }
}

//TextField Component

@Composable
fun TextFieldScreen() {
    Column(modifier = Modifier
        .padding(16.dp)
        .fillMaxSize()) {
        TextFieldComponent()
        CommonSpace()
        OutlineTextFieldComponent()
        CommonSpace()
        PasswordTextFieldComponent()
    }
}

@Composable
fun TextFieldComponent() {
    var firstName by remember {
        mutableStateOf("")
    }
    val keyboardController = LocalSoftwareKeyboardController.current
    TextField(
        value = firstName,
        onValueChange = {
            firstName = it
        },
        textStyle = TextStyle(
            color = Color.Green,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold
        ),
        label = {
            Text(text = "Firstname", color = Color.Gray)
        },
        placeholder = {
            Text(text = "Enter your firstname", color = Color.Gray)
        },
        leadingIcon = {
            Icon(Icons.Default.Person, contentDescription = "Person Icon")
        },
        trailingIcon = {
            IconButton(onClick = {
                firstName = ""
            }) {
                Icon(Icons.Default.Close, contentDescription = "Close Icon")
            }
        },
        colors = TextFieldDefaults.colors(
            focusedContainerColor = Color.Black,
            unfocusedContainerColor = Color.Black,
            cursorColor = Color.Blue,
            errorCursorColor = Color.Red,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            errorIndicatorColor = Color.Transparent,
            unfocusedLeadingIconColor = Color.White,
            unfocusedTrailingIconColor = Color.White
        ),
        shape = RoundedCornerShape(16.dp),
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Text,
            capitalization = KeyboardCapitalization.Sentences
        ),
        keyboardActions = KeyboardActions(
            onDone = {
                keyboardController?.hide()
            }
        )
    )
}

@Composable
fun OutlineTextFieldComponent() {
    var email by remember {
        mutableStateOf("")
    }

    OutlinedTextField(
        value = email,
        onValueChange = {
            email = it
        },
        label = {
            Text(text = "Username/Email")
        },
        leadingIcon = {
            Icon(Icons.Default.Email, contentDescription = "Email Icon")
        }
    )
}

@Composable
fun PasswordTextFieldComponent() {
    var password by remember {
        mutableStateOf("")
    }
    var isShowPassword by remember {
        mutableStateOf(false)
    }
    OutlinedTextField(
        value = password,
        onValueChange = {
            password = it
        },
        label = {
            Text(text = "Password")
        },
        leadingIcon = {
            Icon(Icons.Default.Lock, contentDescription = "Password Icon")
        },
        trailingIcon = {
            IconButton(onClick = {
                isShowPassword = !isShowPassword
            }) {
                Icon(if(isShowPassword) Icons.Outlined.KeyboardArrowRight else Icons.Outlined.KeyboardArrowLeft, contentDescription = "Eye Icon")
            }
        },
        visualTransformation = if(isShowPassword) VisualTransformation.None else PasswordVisualTransformation()
    )
}

//Box Layout

@Composable
fun BoxScreen() {
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        BoxItem(color = Color.Blue, size = 180.dp, modifier = Modifier.align(Alignment.TopStart))
        BoxItem(color = Color.Red, size = 160.dp, modifier = Modifier.align(Alignment.TopCenter))
        BoxItem(color = Color.Yellow, size = 140.dp, modifier = Modifier.align(Alignment.TopEnd))
    }
}

@Composable
fun BoxItem(modifier: Modifier = Modifier, color: Color, size: Dp = 100.dp) {
    Box(modifier = modifier
        .size(size)
        .background(color)
    )
}

@Composable
fun CommonSpace() {
    Spacer(modifier = Modifier.height(16.dp))
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun GreetingPreview() {
    JetPackComposeExampleTheme {
        BoxScreen()
    }
}